package org.levelup.lesson3;

public class PointDistanceCalculation {

    // тип возвращаемого значения - double
    double calculateDistance(Point p1, Point p2) {
        double k1 = Math.pow(p1.x - p2.x, 2); // 1 parameter - число, которое мы хотим возвести в степень
        double k2 = Math.pow(p1.y - p2.y, 2);
        // double result = Math.sqrt(k1 + k2); // sqrt - взятие квадратного корня из числа
        // return result;
        return Math.sqrt(k1 + k2);
    }

    double calculate(Point p1, Point p2) {
        double k1 = p1.x - p2.x; // 1 parameter - число, которое мы хотим возвести в степень
        double k2 = p1.y - p2.y;

        return k1 + k2;
    }

    // class Point {
    //      int x;
    //      int y;
    // }

}
