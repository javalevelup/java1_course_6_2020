package org.levelup.lesson3;

public class PointService {

    void changeCoordinates(Point point, int x, int y) {
        if (x >= 0 && y >=0) {
            point.x = x;
            point.y = y;
        }
    }

    void changeCoordinates(int x, int y) {

    }

}
