package org.levelup.lesson3;

// Point{} - 16B
// Point{int x;} - 24B
// Point{int x; int y} - 24B
// Point{int x; long y} - 32B
public class Point {

    // Поле класса, field
    int x;
    int y;
    // int[] z;

    // Контсруктор класса - специальный метод
    // Он не имеет типа возвращаемого значения
    // Название коструктора совпадает с именем класса

    // Конструктор по умолчанию
    // public <Имя_класса>() {}
    // public Point() {}

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Point() {
        this.x = 1;
        this.y = 1;
    }

    // метод для изменения значений x и y
    // <тип возвращаемого значания> <название_метода>(<тип_аргумента1> <наз.аргумента>, <тип_аргумента2> <наз.аргумента2>,
    //                  <тип_аргумента3> <наз.аргумента3> ...) {
    //      тело метода - то, что метод делает
    // }
    // тип void - метод не возвращает результат
    void changeCoordinates(int x, int y) {
        if (x >= 0 && y >= 0) {
            this.x = x; // pointA.x = x; || pointB.x = x;
            this.y = y;
        }
    }

    // Этот метод не имеет параметров (аргументов)
    void printPoint() {
        // "" - пустая строка
        // System.out.println("" + "(" + x + ", " + y + ")");
        this.printPoint("");
    }

    // Каждый метод имеет сигнатуру
    // Сигнатура - имя метода и его параметры (аргументы), причем важен как порядок аргументов, так и их типы
    // Перегрузка методов (method overloading) - методы имеют одинаковые названия, но разные сигнатуры
    void printPoint(String pointName) {
        // this
        System.out.println(pointName + "(" + this.x + ", " + y + ")");
    }

    // int m(int a, double b) << есть в классе - m(int, double)
    //
    // 1. int m(int a, int b)           - yes - m(int, int)
    // 2. void m(int a, double b)       - no  - m(int, double)
    // 3. int m(double a, int b)        - yes - m(double, int)
    // 4. int m(int b, double a)        - no  - m(int, double)
    // 5. int m(double a, double b)     - yes - m(double, double)
    // 6. double m(int b, double a)     - no  - m(int, double)

    double calculateDistance(Point second) {
        double k1 = Math.pow(x - second.x, 2);
        double k2 = Math.pow(y - second.y, 2);

        return Math.sqrt(k1 + k2);
    }

    double calculate(Point second) {
        double k1 = this.x - second.x;
        double k2 = this.y - second.y;

        return k1 + k2;
    }


}
