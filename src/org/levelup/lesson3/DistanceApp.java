package org.levelup.lesson3;

public class DistanceApp {

    public static void main(String[] args) {
        // Метод, который рассчитывает расстояние между двумя точками
        Point p1 = new Point(4, 6);
        Point p2 = new Point(8, 9);

        // 1 способ
        // calculateDistance находится в классе Point
        // double calculateDistance(Point p2)

        // p1.calculateDistance(p2)
        // p2.calculateDistance(p1)

        double result = p1.calculateDistance(p2); // x,y берутся из объекта p1
        double r = p1.calculate(p2);
        // p2.calculateDistance(p1) -> x,y берутся из объекта p2
        System.out.println("Расстояние между точками (ООП): " + result);

        // 2 способ
        // calculateDistance находится в другом классе (не в Point), к примеру, в PointService
        // double calculateDistance(Point p1, Point p2)
        // double calculateDistance(int x1, int y1, int x2, int y2)

        PointDistanceCalculation calc = new PointDistanceCalculation();
        double distance = calc.calculateDistance(p1, p2);
        double d = calc.calculate(p1, p2);

        System.out.println("Расстояние между двумя точками: " + distance);

    }

}
