package org.levelup.lesson3;

public class App {

    public static void main(String[] args) {

        int[] xValues = new int[3];
        int[] yValues = new int[3];
        int[] zValues = new int[3];
        // A(x[0], y[0])

        int i = 0; // i - переменная

        // pointA - объект (переменной ссылочного типа), ссылкой, экземпляр (класса), object, reference, instance
        Point pointA = new Point(); // a42124bacd4
        // запись значения в поле объекта
        pointA.x = 453;
        pointA.y = 1353;

        System.out.println("A("+ pointA.x + ", " + pointA.y + ")");

        Point pointB = new Point();
        pointB.x = 12;
        pointB.y = 43;

        System.out.println("B(" + pointB.x + ", " + pointB.y + ")");

        // pointA.x = ...
        // pointA.y = ...
        pointA.changeCoordinates(16, 14); // << вызов метода changeCoordinates
        System.out.println("A*("+ pointA.x + ", " + pointA.y + ")");

        // DRY - don't repeat yourself
        pointA.changeCoordinates(-12, -26); // << вызов метода changeCoordinates
        System.out.println("A**("+ pointA.x + ", " + pointA.y + ")");

        System.out.println();
        pointA.printPoint();
        pointB.printPoint("B");

        Point pointC = new Point(35, 8);
        pointC.printPoint("C");

        PointService ps = new PointService();
        ps.changeCoordinates(pointC, 13, 12);

    }

}
