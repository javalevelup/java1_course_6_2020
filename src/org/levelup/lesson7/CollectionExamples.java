package org.levelup.lesson7;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CollectionExamples {

    public static void main(String[] args) {

        // List<List<String>> listOfStrings;
        // List<String> list = listOfString.get(0);

        List<String> words = new LinkedList<>();
        // List<String> words = new ArrayList<>();
        // ArrayList<String> words = new ArrayList<>();

        words.add("Hello");
        words.add("Java");
        words.add("Payments");
        words.add("Presentation");
        words.add("Analyze");

        // foreach
        // for (<Generic type> <var> : <collection object>)
        for (String word : words) {
            // НЕЛЬЗЯ! добавлять или удалять элементы из коллекции во время цикла
            // Будет выброшено ConcurrentModificationException
            System.out.println(word);
        }
        System.out.println("Размер коллекции: " + words.size());

        // start - включен (included)
        // end - исключен (excluded)
        List<String> subwords = words.subList(1, 4);  // 1ый, 2ой и 3ий элементы (если смореть по индексам) -- элементы с инденсами 1, 2 и 3
        for (String word : subwords) {
            System.out.println(word);
        }

        String firstWord = words.get(0);
        System.out.println("Первый элемент: " + firstWord);

    }

}
