package org.levelup.lesson7;

// F - first type
// S - second type
public class GenericPair<F, S> {

    void addPair(F first, S second) {

    }

    S getPairByFirstValue(F first) {
        return null;
    }

    F getPairBySecondValue(S second) {
        return null;
    }

}
