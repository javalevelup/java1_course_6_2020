package org.levelup.lesson7;

public class GenericObject<TYPE> {

    private TYPE field;

    public void setField(TYPE field) {
        this.field = field;
    }

    public TYPE getField() {
        return field;
    }

    public int getIntValue() {
        return 0;
    }

}
