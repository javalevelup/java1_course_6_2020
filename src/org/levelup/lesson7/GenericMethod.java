package org.levelup.lesson7;

public class GenericMethod {

    private Object[] array;

    public GenericMethod() {
        this.array = new Object[] {
                "Hello",
                35.43d,
                685
        };
    }

    public <TYPE> TYPE findByIndex(int index) {
        //noinspection unchecked
        return (TYPE) array[index];
    }

}
