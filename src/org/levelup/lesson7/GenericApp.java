package org.levelup.lesson7;

import org.levelup.lesson4.Shape;

public class GenericApp {

    public static void main(String[] args) {
        GenericObject<String> object = new GenericObject<>();
        object.setField("");
        object.setField("new Shape()");

        GenericObject<Integer> integerObject = new GenericObject<>();
        integerObject.setField(405);

        GenericObject<Object> genericObject = new GenericObject<>();
        genericObject.setField(new Object());

        // GenericObject<Number> numberObject = new GenericObject<>();
        // numberObject.setField(342.3);


        GenericPair<String, Integer> pair = new GenericPair<>();
        pair.addPair("hello", 45);
        String firstValue = pair.getPairBySecondValue(45);

        GenericPair<String, String> stringPairs = new GenericPair<>();
        stringPairs.addPair("hello", "привет");

        GenericMethod method = new GenericMethod();
        double value =  method.findByIndex(1);
        int intValue = method.findByIndex(2);
        String sValue = method.findByIndex(0);



    }

}
