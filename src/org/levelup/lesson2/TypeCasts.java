package org.levelup.lesson2;

@SuppressWarnings("ALL")
public class TypeCasts {

    // psvm/main + Tab/Enter
    public static void main(String[] args) {
        int intVar = 493;
        long longVar = intVar; // неявное расширяющее преобразование

        short shortVar = (short) longVar; // явное сужающее преобразование
        // 0.1 + 0.2 = 0.3 ? -> 0.2999999999999999999999999 != 0.3, 0.3000000000000000001 != 0.3

        int value = 130;
        // [-128, 127]
        byte byteValue = (byte) value; // -126
        // int - byte
        // 127 - 127
        // 128 - -128
        // 129 - -127
        // 130 - -126
        System.out.println(byteValue);

//        int v = 128 >>> 3; // 000010110 >>> 1 = 000001011
//        int v1 = 30 & 4;
//        System.out.println(v);
//        System.out.println(v1);
    }

}
