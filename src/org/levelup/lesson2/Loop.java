package org.levelup.lesson2;

public class Loop {

    public static void main(String[] args) {

        // Вывести квадраты чисел от 0 до 30
        for (int i = 0; i < 30; i++) {
            System.out.println(i * i);
        }

        System.out.println();

        for (int i = 1000; i > 1; i = i - 100) {
            System.out.println(Math.sqrt(i));
        }

    }

}
