package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) {
        // Считываение данных с клавиатуры (мы позволим вводить данные)
        // Scanner
        Scanner sc = new Scanner(System.in);
        // sout
        System.out.println("Введите число:");
        // Считываем число с клавиатуры
        // int number = sc.nextInt();
        int number;

        // System.out.println(secretNumber);

        // Генерация псевдо-случайных чисел
        Random r = new Random();
        // nextInt(4) -> [0, 4) || [0, 3]
        // nextInt(4) + 1 -> [1, 5) || [1, 4]
        int secretNumber = r.nextInt(4) + 1;

        // for (;;)
        // for (; number != secretNumber; ) {}
//        while (number != secretNumber) {
//            System.out.println("Вы не угадали!");
//            System.out.println("Введите число заново:");
//            number = sc.nextInt();
//        }
//        System.out.println("Вы угадали!");

        do {
            System.out.println("Введите число:");
            number = sc.nextInt();
        } while (number != secretNumber);
        System.out.println("Вы угадали!");

//        if (number == secretNumber) { // == - проверка равенства, != - проверка неравенства
//            System.out.println("Вы угадали!");
//        } else {
//            // System.out.println("Вы не угадали! Секретное число: " + secretNumber);
//            if (number > secretNumber) {
//                System.out.println("Вы ввели число больше чем загаданное");
//            } else { // если число < или =
//                System.out.println("Вы ввели число меньше чем загаданное");
//            }
//        }


//        if (number == secretNumber) { // == - проверка равенства, != - проверка неравенства
//            System.out.println("Вы угадали!");
//            // int var = 34;
//        } else if (number > secretNumber) {
////            int var = 23;
//            System.out.println("Вы ввели число больше чем загаданное " + secretNumber);
//        } else {
//            System.out.println("Вы ввели число меньше чем загаданное");
//        }

//        if (number == secretNumber) {
//            System.out.println("Вы угадали!");
//        }
//        if (number > secretNumber) {
//            System.out.println("Вы ввели число больше чем загаданное");
//        }
//        if (number < secretNumber) {
//            System.out.println("Вы ввели число меньше чем загаданное");
//        }

    }

}
