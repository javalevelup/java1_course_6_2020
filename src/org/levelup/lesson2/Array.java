package org.levelup.lesson2;

import java.util.Arrays;

public class Array {

    public static void main(String[] args) {

        // price1
        // price2
        // price3
        // avg = (price1 + price2 + price3) / 3

        // <тип элементов в массиве>[] <название переменной>
        double[] prices = new double[3]; // 3 - длина массива - количество элементов в массиве
        // записать значение в массив (в какой-то элемент)
        prices[0] = 2353.4; // 0 - первый элемент массива
        prices[1] = 875.56;
        prices[2] = 64.31;
        // prices[3] = 324.1; ArrayIndexOutOfBoundException - мы вышли за пределы массива
        // [0], [1], [2] - индексы в массиве
        System.out.println(prices[0]); // выведу первый элемент массива

        double priceSum = 0;
        for (int idx = 0; idx < prices.length; idx++) {
            System.out.println(prices[idx]);
            priceSum = priceSum + prices[idx];
        }

        double averagePrice = priceSum / prices.length;
        System.out.println("Average price: " + averagePrice);

        System.out.println(Arrays.toString(prices));

        // int[][] matrix = new int[3][3];
        // int[][][] matrix3D = new int[3][3][3];



    }

}
