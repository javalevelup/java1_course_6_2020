package org.levelup.lesson6;

@SuppressWarnings("ALL")
public class IntegerPool {

    public static void main(String[] args) {
        double d; Double dRef;
        long l; Long lRef;

        int i; Integer iRef;
        char c; Character cRef;

        int d2 = 127;
        Integer i1 = d2; // autoboxing
        Integer i2 = 127;

        Integer s = i1 + i2;
        i2 = 126;

        //Integer i2 = Integer.valueOf(127); // Integer.valueOf(127);
        // i2.intValue(); // 127

        m(124);
        m(i1); // m(i1.intValue());

        Integer i3 = 129;
        Integer i4 = 129;

        int i5 = i1; // autounboxing  i1.intValue();

        System.out.println(i1 == i2); // true
        System.out.println(i3 == i4); // false

    }

    static void m(int a) {

    }

}
