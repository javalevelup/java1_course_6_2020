package org.levelup.lesson6;

@SuppressWarnings("ALL")
public class WhatIsIt {

    public static void main(String[] args) {
        int value = 10;
        Point p1 = new Point(10);
        Point p2 = new Point(10);

        value = changePrimitive(value);
        changePointDirectly(p1);
        changePointUsingSetter(p2);

        System.out.println(value);         // 10 - 10
        System.out.println(p1.x);          // 10 - 20
        System.out.println(p2.getX());     // 20 - 20

    }

    static int changePrimitive(int primitiveValue) {
        primitiveValue = 20;
        return primitiveValue;
    }

    static void changePointDirectly(Point p) {
        p.x = 20;
    }

    static void changePointUsingSetter(Point p) {
        p.setX(20);
    }

}
