package org.levelup.lesson6.structure;

// 1. Все методы абстрактны
// 2. Все методы публичные (public)
// 3. Все поля интерфейса имеют следующий вид
//      public static final
// 4. У интерфейса нет конструкторов
// 5. Один класс может реализовывать несколько интерфейсов
// 6. Описание того, что может делать ваш класс (какие методы у него могут быть)
public interface Structure<T> {

    // public static final int size = 50;
    // int size = 50;

    // public abstract void add(int value);
    void add(T value);

    void removeByValue(T value);

    // true, если value есть в структуре
    boolean contains(T value);

}
