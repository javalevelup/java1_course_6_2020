package org.levelup.lesson6.structure;

public class StructureApp {

    public static void main(String[] args) {

        Structure<String> list = new OneWayList<>();
        list.add("Hello");
        list.add("World");
        list.add("Java");
        list.add("List");
        list.add("Element");


        // Shape s = new Rectangle();

        // Structure s = new Structure(); <- нельзя
        // Structure structure = new DynamicArray(5);
        // DynamicArray structure = new DynamicArray(5);
        // Structure structure = new DynamicArray(5); // new Queue(); new Stack();
        AbstractStructure structure = new DynamicArray(5);

        structure.addLast(45);
        structure.addLast(0);
        structure.addLast(34);
        structure.addLast(54);
        structure.addLast(78);
        structure.addLast(76);
        structure.addLast(0);
        structure.addLast(12);
        structure.addLast(43);

    }

}
