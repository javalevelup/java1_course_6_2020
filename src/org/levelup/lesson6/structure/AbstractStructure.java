package org.levelup.lesson6.structure;

// Нельзя создать объект абстрактного класса
public abstract class AbstractStructure {

    protected int size; // количество элементов в нашей структуре данных

    public AbstractStructure() {}

    public abstract void addLast(int value);

    // Удаление элемента из структуры по иднексу
    public void removeByIndex(int index) {}

    public int getByIndex(int index) { return 0; }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

}
