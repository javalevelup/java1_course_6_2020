package org.levelup.lesson6.structure;

// Динамический массив (Список на основе массива)
public class DynamicArray extends AbstractStructure {

    private int[] elementData; // в этом массиве мы храним значения структуры

    public DynamicArray(int initialCapacity) {
        this.elementData = new int[initialCapacity];
        this.size = 0;
    }

    // new int[4]
    // [0, 0, 0, 0] - size = 0
    // 1. addLast(40) -> [40, 0, 0, 0] - array.length = 4, size = 1
    // 2. addLast(65) -> [40, 65, 0, 0] - array.length = 4, size = 2
    // 3. addLast(89) -> [40, 65, 89, 0] - array.length = 4, size = 3
    // 4. addLast(69) -> [40, 65, 89, 69] - array.length = 4, size = 4
    // 5. addLast(74) -> [40, 65, 89, 69] -> [40, 65, 89, 69, 74, 0, 0, 0] - array.length = 8, size = 5
    // 6. addLast(0) ->  [40, 65, 89, 69, 74, 0, 0, 0] - array.length = 8, size = 6
    // 7. addLast(62) -> [40, 65, 89, 69, 74, 0, 62, 0] - array.length = 8, size = 7
    @Override
    public void addLast(int value) {
        if (elementData.length == size) {
            // места нет, нужно увеличить размер массива
            int[] oldArray = elementData;
            elementData = new int[(int) (size * 1.5)];

            System.arraycopy(oldArray, 0, elementData, 0, oldArray.length);
        }

        elementData[size++] = value; // куда вставлять элементы
        // elementData[size] = value;
        // size = size + 1;
    }

}
