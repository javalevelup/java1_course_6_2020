package org.levelup.lesson1;

// full name: org.levelup.lesson1.PrimitiveTypes
public class PrimitiveTypes {

    public static void main(String[] args) {
        // <тип переменное> <название переменной>
        int countOfProducts; // объявление переменной
        countOfProducts = 364; // присвоили переменной значение 364
        System.out.println(countOfProducts);

        int price = 5834; // инициализация переменной
        int total = price * countOfProducts;

        int sum = countOfProducts + 4904;
        System.out.println(sum);

        // + c использованием строк
        // Конкатенация строк - сложение (слияние) строк
        // Строка + число => Строка + Строка => СтрокаСтрока
        System.out.println("Итого: " + total); // Итого <значение переменной total>

        int a = 10;
        int b = 10;
        System.out.println("Сумма: " + (a + b));
        System.out.println("Сумма: " + a + b); // Строка + число + число -> СтрокаЧисло + число -> СтрокаЧислоЧисло: "Сумма: 1010"
        // "Сумма: " + 10 + 10
        // "Сумма: " + a = "Сумма: " + 10 = "Сумма: " + "10" = "Сумма: 10"
        // "Сумма: 10" + b = "Сумма: 10" + "10" = "Сумма: 1010"
        System.out.println(a + b + ": сумма"); // Число + число + Строка -> Число + строка -> ЧислоСтрока: "20: сумма"
        // 10 + 10 + ": сумма"
        // a + b = 10 + 10
        // a + b + ": сумма" = 20 + ": сумма" = "20" + ": сумма" = "20 : сумма"
        System.out.println(a + " :сумма: " + b); // "10 :сумма: 10"

        double d = 304.3;
        System.out.println("double value: " + d);

        int s = 10;
        // increment/decrement
        // s++ => s = s + 1
        // s-- => s = s - 1
        // Префиксный инкримет (++s) - сначала выполнить икремент, потом выполнить оставшиеся операция
        int val = a + ++s;
        // s = s + 1
        // int val = a + s

        // Постфиксный инкримет (s++) - сначала выполнить все другие операции, потом выполнить икремент
        int v = a + s++;
        // int v = a + s;
        // s = s + 1;
        s++;
        ++s;

        int r = 30;
        System.out.println(r++);
        // System.out.println(r)
        // r = r + 1
        System.out.println(r); // 31
        System.out.println(++r);
        // r = r + 1
        // System.out.println(r)

        // r = 30
        // System.out.println(r) 30
        // r = r + 1 = 31
        // r = r + 1 = 32
        // System.out.println(r) 32

    }

}
