package org.levelup.lesson9.file;

import java.io.File;
import java.io.IOException;

@SuppressWarnings("ALL")
public class FileExample {

    public static void main(String[] args) throws IOException {

        File textFile = new File("text_file.txt");
        File notExistingFile = new File("test.txt");

        System.out.println(textFile == null);           // false | false
        System.out.println(notExistingFile == null);    // true  | false

        System.out.println(textFile.exists());          // true - файл существует на фс
        System.out.println(notExistingFile.exists());   // false - файла нет на фс

        if (!notExistingFile.exists()) {
            boolean result = notExistingFile.createNewFile();
            System.out.println("Результат создания файла: " + result);
        }
        // notExistingFile.createNewFile();

        File srcDir = new File("src/");
        System.out.println("Is directory exist: " + srcDir.exists());

        System.out.println("Is directory: " + srcDir.isDirectory());
        System.out.println("Is file: " + srcDir.isFile());

        File tempDir = new File("temp/");
        boolean result = tempDir.mkdir(); // make directory - создание папки
        System.out.println(result);

    }

}
