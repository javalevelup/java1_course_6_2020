package org.levelup.lesson9.file;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadConsole {

    public static void main(String[] args) throws IOException {
        // System.in - InputStream - чтение ввода с клавиатуры
        // System.out/System.err - вывод в консоль с приложение

        // InputStreamReader: convert InputStream to Reader
        // OutputStreamWriter: convert OutputStream to Writer

        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        // consoleReader.close();
        // consoleReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter you code...");
        String line = consoleReader.readLine();

        for (int i = 0; i < line.length(); i++) {
            char symbol = line.charAt(i);
            if (symbol < 48 || symbol > 57) {
                System.out.println("Вы ввели неправильные цифры");
                return;
            }
        }

        int code = Integer.parseInt(line);
        System.out.println("Code as int: " + code);

    }

}
