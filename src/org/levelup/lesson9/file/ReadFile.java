package org.levelup.lesson9.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadFile {

    // InputStream
    // OutputStream

    // Reader
    // Writer

    public void readAndPrintFile(File file) {
        // FileReader / FileInputStream
        // FileReader fileReader = new FileReader(file);
        // fileReader.read();// acsi code
        // char[] buf = new char[1024];
        // int readCount = fileReader.read(buf); // readCount - количество символов, которые мы прочитали
        // int readCount = fileReader.read(buf, 100, 150); // 150 символов, начиная с 100 [100, 250]

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file)); // new FileReader -> throw new FileNotFoundException

            String line;
            while ( (line = reader.readLine()) != null ) {
                System.out.println(line);
            }

        } catch (IOException exc) {
            System.out.println("Произошла ошибка при чтении файла: " + exc.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException exc) {
                    System.out.println("Невозможно закрыть соединение к файлу.");
                }
            }
        }
    }

    public void readAndPrintFileAsStream(File file) {
        // try-with-resources
        // Класс, обязан реализовывать интерфейс AutoClosable

        // try (создаете объект, который реализует интерфейс AutoClosable) {}

        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {

            // byte[] buffer = new byte[2048];
            byte[] buffer;
            while ( (buffer = bis.readNBytes(100)).length != 0 ) {
                // byte[] -> String: new String(bytes);
                // String -> byte[]: string.getBytes();
                System.out.println(new String(buffer));
                System.out.println("----");
            }

        } catch (IOException exc) {
            System.out.println("Произошла ошибка при чтении файла: " + exc.getMessage());
        }

    }

    public void writeToFile(File file, String message) {
        // new FileWriter(File f, boolean append)
        // по умолчанию, append = false - означает, что нужно очистить файл перед записью
        // append = true - добавляем новый текст к тому тексту, который есть в файле
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {

            writer.write(message);
            writer.newLine();

            // writer.write(message + "\n");

        } catch (IOException exc) {
            System.out.println("Произошла ошибка при записи в файл: " + exc.getMessage());
        }
    }

}
