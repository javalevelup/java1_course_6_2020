package org.levelup.lesson9;

// unchecked exception
// EmptyStackException -> RuntimeException -> Exception
public class EmptyStackException extends RuntimeException {

    public int value;

    public EmptyStackException() {
        super("Stack is empty");
    }

}
