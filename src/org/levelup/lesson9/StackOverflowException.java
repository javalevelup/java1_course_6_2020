package org.levelup.lesson9;

// checked exception
public class StackOverflowException extends Exception {

    public StackOverflowException(int capacity) {
        super("Stack if full. Stack capacity is " + capacity);
    }

}
