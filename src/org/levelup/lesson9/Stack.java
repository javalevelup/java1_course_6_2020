package org.levelup.lesson9;

// LIFO - last in, first out
public class Stack<T> {

    // +(a, b) ~ a + b
    // a + b + (c * d + 1)

    // push(value) - положить значение на вершину стека
    // value pop - взять значение с вершины стека
    // value peek - вернуть значение с вершины стека

    private Object[] elements;
    private int headIndex;

    public Stack(int capacity) {
        // this.elements = new T[capacity];
        this.elements = new Object[capacity];
    }

    public void push(T value) throws StackOverflowException {
        if (headIndex == elements.length) {
            // если стек заполнен, то выбрасываем исключение
            throw new StackOverflowException(elements.length);
        }
        elements[headIndex++] = value;
    }


    public T pop() {
        if (headIndex == 0) {
            // в стеке нет элементов, т.е. он пустой
            throw new EmptyStackException();
        }
        return (T) elements[--headIndex];
    }

}
