package org.levelup.lesson9;

public class StackApp {

    public static void main(String[] args) {
        Stack<String> stack = new Stack<>(5);

        try {
            stack.push("string_1");
//            stack.push("string_2");
//            stack.push("string_3");
//            stack.push("string_4");
//            stack.push("string_5");
//            stack.push("string_6");
            String string = stack.pop();
            String string2 = stack.pop();
        } catch (StackOverflowException exc) {
            System.out.println(exc.getMessage());
        } catch (EmptyStackException | NullPointerException exc) {
            System.out.println("Стек пустой");
            exc.printStackTrace();
        } catch (Exception exc) {
            System.out.println("Something get wrong...");
        }

    }

}
