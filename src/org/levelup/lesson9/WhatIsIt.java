package org.levelup.lesson9;

@SuppressWarnings("ALL")
public class WhatIsIt {

    public static void main(String[] args) {
        // int val = method(10);
        // System.out.println(val);

        int val = returnIntValue();
        System.out.println(val); // 2 or 0
    }

    static int method(int value) {
        try {
            if (value == 10) {
                throw new RuntimeException();
            }
            System.out.println("try block");
            return value;
        } catch (Exception exc) {
            System.out.println("catch block");
            return 0;
        } finally {
            System.out.println("finally block");
        }
    }

    static int returnIntValue() {
        try {
            // throw new RuntimeException();
            // System.exit(0);
            return 0;
        } catch (Exception exc) {
            return 1;
            // return 2;
        } finally {
            return 2;
        }
    }

    static void finallyArguments() {
        int code = 0;
        try {
            code = 45;
            int var = 4;
        } finally {
            // var = 5; - нельзя обратиться к переменной, объявленной в блоке try
            if (code != 0) {
                System.out.println("Code doesn't equal to zero");
                // method1();
            }
        }
    }

}
