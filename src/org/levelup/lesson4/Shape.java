package org.levelup.lesson4;

// 2 модификатора доступа для внешних классов (у которых имя класса совпадает с именем файла, в котором он описан)
// public - класс может быть использован везде
// default-package (private-package) - класс может испольпользоваться только в пакете
public class Shape {

    // Модификатор доступа
    // private - мы можем вызывать метод только внутри класса/мы можем обращаться к полю только внутри класса
    // default-package (private-package) - (мы не указали модификатор доступа) - доступ есть только внутри класса и пакета
    // protected - доступ есть только внутри класса и пакета, а также в любых классах-наследниках
    // public - доступ есть везде

    int[] sides; // значения сторон фигуры

    public Shape() {
        super();
        System.out.println("Вызван конструктор Shape");
        this.sides = new int[0]; // пустой массив
    }

    public Shape(int[] sides) {
        this.sides = sides;
    }

    public double calculatePerimeter() {
        return 0;
    }

}
