package org.levelup.lesson4;

@SuppressWarnings("ALL")
public class CastExample {

    public static void main(String[] args) {

        Square square = new Square(4);
        System.out.println("Сторона квадрата: " + square.getSide());

        Rectangle rectangle = new Rectangle(4, 5);
        Rectangle rec = new Rectangle(5, 4);

        Shape shape = new Shape(new int[] {4, 6, 8, 8, 9});

        Shape[] shapes = new Shape[4];
        shapes[0] = shape;
        shapes[1] = rec; // Shape s = rec; shapes[1] = s;
        shapes[2] = rectangle;
        shapes[3] = square;

        ShapeService ss = new ShapeService();
        ss.printShapesPerimeters(shapes);

        // Square -> Rectangle -> Shape -> Object
        // Square -> Shape

        // int i = 43;
        // long l = i;

        Rectangle rect = new Rectangle(3, 5);
        rect.sides[0] = 5;


        Square s = new Square(4);
        // s = (Rectangle) rect;
        s.sides[0] = 7;
        Rectangle r = s;
        rect = s; //

        s.sides[0] = 90; // r.sides[0] == 90;
        r.sides[0] = 91; // s.sides[0] == 91;
        //   r.sides[0] == 7;


//        Rectangle recObjFromSquare = square;
//        square.getSide();
//        recObjFromSquare.getSide();
//
//
//
//
//
//        Shape shapeObjFromRectangle = recObjFromSquare;
//        Object objFromSquare = square;
//        Rectangle recFromObject = (Rectangle) objFromSquare;
//        Shape shapeFromObject = (Shape) objFromSquare;
//
//        Square s = (Square) rectangle; // ClassCastException

    }

}
