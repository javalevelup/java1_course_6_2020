package org.levelup.lesson4;

public class ShapeService {

    public void printShapesPerimeters(Shape[] shapes) {
        //
        for (int i = 0; i < shapes.length; i++) {
            // shape[0] - Shape -> calculatePerimeter из класса Shape
            // shape[1] - Rectangle -> calculatePerimeter из класса Rectangle
            // shape[2] - Rectangle -> calculatePerimeter из класса Rectangle
            // shape[3] - Square -> calculatePerimeter из класса Square
            System.out.println(shapes[i].calculatePerimeter());
        }
    }

    // printShapePerimeter(Shape[] shapes)
    // printRectanglePerimeter(Rectangle[] shapes)
    // printSquarePerimeter(Square[] shapes)

}
