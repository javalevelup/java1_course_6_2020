package org.levelup.lesson4;

// Класс, от которого наследуются - base class (базовый класс), superclass
// Класс, который является наследником - subclass (подкласс)
// Inheritance (Наследование) - is as
public class Rectangle extends Shape {

    public Rectangle() {
        super(); // вызов конструктора суперкласс
        System.out.println("Вызван конструктор Rectangle");
    }

    public Rectangle(int width, int length) {
        // int[] arr = new int[4];

        // int[] arr = { 2, 5, 7 };
        //      int[] arr = new int[3];
        //      arr[0] = 2;
        //      arr[1] = 5;
        //      arr[2] = 7;

        // method(new int[] { 2, 5, 7 }) - создать массив и передать в метод;
        super(new int[]{width, length, width, length});
    }

    // Overriding (Переопределение метода)
    @Override
    public double calculatePerimeter() {
        // width * 2 + length * 2
        return sides[0] * 2 + sides[1] * 2;
    }

}
