package org.levelup.lesson8.exceptions;

import java.text.ParseException;
import java.util.Date;

public class ExceptionApp {

    public static void main(String[] args) throws ParseException {
        DateConverter converter = new DateConverter();

        String string = "23.01.2021 11:45:00";

        Date date = converter.convertStringToDate(string);
        System.out.println(date);

    }

}
