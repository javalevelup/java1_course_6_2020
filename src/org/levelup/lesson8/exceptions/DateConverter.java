package org.levelup.lesson8.exceptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

    public Date convertStringToDate(String dateAsString) throws ParseException {
        // Date - as long - количество милисекунд, которые прошли с 1 января 1970 с 00:00.
        // 22.01.2021 12:00:00

        // d - from 1 - 9 -> 1 - 9
        // dd - from 1 - 9 -> 01 - 09
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        try {
            System.out.println("Before parse");
            Date date = formatter.parse(dateAsString);
            System.out.println("After parse");
            // return date;
        } catch (ParseException exc) {
            System.out.println("Неправильный формат строки");
            exc.printStackTrace();
            return null;
        }


        throw new ParseException("Я захотел бросить исключение!", 0);
    }

}
