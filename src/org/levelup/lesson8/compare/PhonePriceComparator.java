package org.levelup.lesson8.compare;

import java.util.Comparator;

public class PhonePriceComparator implements Comparator<Phone> {

    @Override
    public int compare(Phone o1, Phone o2) {
        // 0.1 + 0.2 == 0.3 ? не факт, что равны

        if (o1.getPrice() < o2.getPrice()) {
            return -1;
        } else if (o1.getPrice() > o2.getPrice()) {
            return 1;
        } else {
            return 0;
        }

        // return Double.compare(o1.getPrice(), o2.getPrice());
    }

}
