package org.levelup.lesson8;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WordCounter {

    public static void main(String[] args) {

        String text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum";

        String[] words = text.replace(".", "")
                .replace(",", "")
                .replace("'s", "")
                .toLowerCase() // переведем все символы в нижний регистр
                .split(" ");

        // Key (String) - word
        // Integer (Value) - count of words
        Map<String, Integer> wordCounter = new HashMap<>();

        for (String word : words) {
            Integer count = wordCounter.get(word);
            if (count == null) {
                // если слово еще не встречалось, то тогда сохраняем его в map
                wordCounter.put(word, 1);
            } else {
                wordCounter.put(word, count + 1);
            }
        }

        Set<Map.Entry<String, Integer>> entries = wordCounter.entrySet();
        // for (Map.Entry<String, Integer> entry : wordCounter.entrySet()) {
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Word: " + entry.getKey() + ", times: " + entry.getValue());
        }

    }

}
