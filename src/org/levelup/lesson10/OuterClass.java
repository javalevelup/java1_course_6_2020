package org.levelup.lesson10;

public class OuterClass {

    public int var;
    public static int staticVar;

    public class InnerClass {

        public InnerClass() {
            System.out.println(var);
        }

    }

    public static class NestedClass {

        public NestedClass() {
            staticVar = 3;
        }

    }

}
