package org.levelup.lesson10;

import org.levelup.lesson6.structure.OneWayList;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ForEachExample {

    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);


        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        for (Integer el : list) {
            System.out.println(el);
//            if (el == 7) {
//                list.remove(el); // new ConcurrentModificationException
//            }
        }

        System.out.println();

        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            Integer element = iterator.next();
            System.out.println(element);
            if (element == 7) {
                // iterator.remove();
                list.remove(element);
            }
        }

        System.out.println();

        OneWayList<String> strings = new OneWayList<>();
        strings.add("s1");
        strings.add("s2");
        strings.add("s3");
        strings.add("s4");

        for (String str : strings) {
            System.out.println(str);
        }

    }

}
