package org.levelup.lesson10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionTasks {

    public static void main(String[] args) {

        Collection<String> products = new ArrayList<>();

        products.add("Мяч");
        products.add("Сетка");
        products.add("Футболка");
        products.add("Штаны");
        products.add("Сыр");
        products.add("Молоко");
        products.add("Вода");
        products.add("Газировка");
        products.add("Ботинки");


        // CollectionTasks.1
        StringPredicate predicate = new StringPredicate() {
            @Override
            public boolean check(String value) {
                return value.startsWith("М");
            }
        }; // аннонимный внутренний класс

        // CollectionTasks.2
        StringPredicate p = new StringPredicate() {
            @Override
            public boolean check(String value) {
                return value.length() > 5;
            }
        };

        Collection<String> filtered1 = filterCollection(products, predicate);
        Collection<String> filtered2 = filterCollection(products, p);

        //
        // StaticExample.value = 45;
        // StaticExample.compareAndSetValue(54);

//        StringPredicate lengthPredicate = (val) -> {
//            return val.length() > 5;
//        };

        StringPredicate lengthPredicate = val -> val.length() > 5;
        filterCollection(products, lengthPredicate);

        filterCollection(products, val -> val.length() > 6);

        // Arguments:
        // 0 аргументы: ()
        // 1 аргумент: val или (val)
        // 2 и более: (arg1, arg2)
        //      ->
        // Тело метода:
        //  { return .... ; }
        //  args -> code  (не будет слова return)

        products.forEach(product -> {
            System.out.println(product);
            System.out.println(product);
        });

        System.out.println();

        products.stream()
                .filter(pr -> pr.startsWith("М"))
                .forEach(pr -> System.out.println(pr));

        System.out.println();

        List<Integer> stringLengths = products.stream()
                .map(pr -> pr.length()) // map(pr -> return pr.length());
                // .map(new Function<String, Integer>() {
                //          @Override
                //          public Integer apply(String t) {
                //              return t.length();
                //          }
                //  });
                .collect(Collectors.toList());
        stringLengths.forEach(l -> System.out.println(l));

        double average = products.stream()
                .mapToInt(pr -> pr.length())
                .average()
                .getAsDouble();
        System.out.println("Средняя длина строки: " + average);

    }

    static boolean f(String value) {
        return value.length() > 4;
    }

    static Collection<String> filterCollection(Collection<String> originalCollection, StringPredicate predicate) {
        Collection<String> filtered = new ArrayList<>();
        for (String element : originalCollection) {
            if (predicate.check(element)) { // проверяет, удовлетворяет ли элемент условию
                filtered.add(element);
            }
        }

        return filtered;
    }

    interface StringPredicate {
        boolean check(String value);
    }

    static class StartWithCertainSymbolStringPredicate implements StringPredicate {
        @Override
        public boolean check(String value) {
            return false;
        }
    }


}
