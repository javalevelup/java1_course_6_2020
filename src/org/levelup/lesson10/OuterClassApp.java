package org.levelup.lesson10;

public class OuterClassApp {

    public static void main(String[] args) {

        OuterClass outerClass = new OuterClass();
        outerClass.var = 10;
        OuterClass.InnerClass innerClass = outerClass.new InnerClass();
        // OuterClass.InnerClass ic = new OuterClass().new InnerClass();

        OuterClass oc = new OuterClass();
        oc.var = 20;
        OuterClass.InnerClass ic = oc.new InnerClass();

        OuterClass.NestedClass nc = new OuterClass.NestedClass();

    }

}
