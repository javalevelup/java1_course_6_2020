package org.levelup.lesson10;

public class StaticExample {

    public static int value = 40;
    public int usualField = 54;

    public static void compareAndSetValue(int newValue) {
        if (newValue < 100) {
            value = newValue;
        }
    }

}
