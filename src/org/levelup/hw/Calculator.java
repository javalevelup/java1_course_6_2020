package org.levelup.hw;

public class Calculator {

    int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    static double sum(double a, double b) {
        double sum = a + b;
        return sum;
    }

    long sum(long a, long b) {
        long sum = a + b;
        return sum;
        // return a + b;
    }

    public static void main(String[] args) {
        int b = 3445;
        long a = 2343452234652435435L; // 2343452234652435 - больше int (Integer.MAX_VALUE)

        Calculator calculator = new Calculator();
        calculator.sum(1, 3); // int sum(int, int)
        calculator.sum(1L, 3L); // long sum(long, long)

        float f = 423.23f;
        double d = 23.54d;

        // sum(1d, 3d);
    }

}
