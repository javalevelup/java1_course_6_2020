package org.levelup.lesson5.comparison;

import java.util.Objects;

// JavaBean
public class Computer {

    private String brand;
    private int ram;
    private long diskSize;

    public Computer(String brand, int ram) {
        this.brand = brand;
        this.ram = ram;
    }

    // getter
    // public <field_type> get<Field_name>() { return <field>; }
    public int getRam() {
        return ram;
    }

    // setter
    // public void set<Field_name>(<field_type> <field_name>) { this.<field_name> = <field_name>; }
    public void setRam(int ram) {
        this.ram = ram;
    }

    public void setDiskSize(long diskSize) {
        this.diskSize = diskSize;
    }

    public long getDiskSize() {
        return diskSize;
    }

    //
    public void changeRAM(int ram) {
        if (ram > 0) {
            this.ram = ram;
        }
    }

    @Override
    public boolean equals(Object object) {
        // Вдруг object - это и есть this
        if (this == object) return true;

        // 2 способа проверки типа
        // 1:
        // intstanceof
        // <obj> instanceof <Class> -> true, если obj имеет тип Class
        // object instanceof Computer
        // null instanceof <Любой класс> -> false
        // if (!(object instanceof Computer)) return false;

        // class A extends Computer
        // A a = new A();
        // a instanceof Computer -> true

        // 2:
        if (object == null || getClass() != object.getClass()) return false;

        // Computer other = (Computer) null -> верно
        Computer other = (Computer) object; // не Computer или не null - упадем с ошибкой ClassCastException

        return ram == other.ram && diskSize == other.diskSize && // NullPointerException
                ((brand == other.brand) || (brand != null && brand.equals(other.brand)));
                // Objects.equals(brand, other.brand);

                // brand.equals(other.brand); // NullPointerException
    }

    // hashCode
    // 1. Если equals для двух объектов возвращает true, то тогда hashCode этих объекты одинаковы
    // 2. Если hashCode объектов одинаковы, то equals для двух объектов не обязательно вернет true
    // if (o1.equals(o2)) => o1.hashCode() = o2.hashCode()
    // if (o1.hashCode() == o2.hashCode()), то o1.equals(o2) может вернуть false
    @Override
    public int hashCode() {
//        int result = 17;
//
//        result = 31 * result + ram;
//        result = 31 * result + (int) diskSize;
//        result = 31 * result + brand.hashCode();
//
        // return result;
        return Objects.hash(ram, diskSize, brand);
    }

}











