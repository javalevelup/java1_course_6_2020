package org.levelup.lesson5.comparison;

import org.levelup.lesson3.Point;

@SuppressWarnings("ALL")
public class ObjectComparison {

    public static void main(String[] args) {

        Computer c1 = new Computer("", 8); // ab32c
        Computer c2 = new Computer("", 8); // 3284d

        Computer c3 = c1; // ab32c

        Computer[] computers = {c1, c2};
        Computer c5 = new Computer("", 9);

        for (int i = 0; i < computers.length; i++) {
            if (c5.hashCode() == computers[i].hashCode() && c5.equals(computers[i])) {
                System.out.println("Такой компьютер есть в списке");
            }
        }


        // boolean isEqual = c1 == c2;
        // System.out.println("c1 == c2 : " + isEqual);
        // == - сравнивает ссылки
        System.out.println("c1 == c3 : " + (c1 == c3)); // -> true
        System.out.println("c1 == c2 : " + (c1 == c2)); // -> false

        boolean isEqual = c1.equals(c2);
        System.out.println("c1.equals(c2): " + isEqual);


        c1.equals(null); // NullPointerException
        c1.equals(c3);


        // null - нулевая ссылка (отсутствие ссылки)
        Computer c4 = null; // null
        if (c4 == null) {
            c4 = new Computer("4", 4);
        }
        // c4.getRam(); -> NullPointerException
        // int r = c4.ram; -> NullPointerException



        // Можно
        // Computer c = new Computer();
        // Object o = c; - o в памяти это все равно Computer
        // Computer other = (Computer) o;

        // Нельзя
        // Object o = new Object();
        // Computer other = (Computer) o; - ошибка

        // c1.equals("Some string");
        // c1.equals(new Point());
        // c1.equals(new Object());

    }

}
