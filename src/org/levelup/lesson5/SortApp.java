package org.levelup.lesson5;

public class SortApp {

    public static void main(String[] args) {
        int[] array = { 1, 2, 3, 4 };

        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(array);

        MergeSort mergeSort = new MergeSort();
        NumberArraySort arraySort = mergeSort; // new MergeSort();
        // NumberArraySort as = (NumberArraySort) new MergeSort();
        arraySort.sort(array); // arraySort - mergeSort


        sortAndPrintArray(array, bubbleSort);
        sortAndPrintArray(array, mergeSort);
        sortAndPrintArray(array, arraySort);

        NumberArraySort ms = getSort("merge");
        NumberArraySort ms2 = getSort("ррор");
        // ms.mergeSort(new double[0]);

    }

    static void sortAndPrintArray(int[] array, NumberArraySort arraySort) {
        arraySort.sort(array);
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

    static NumberArraySort getSort(String sortType) {
        if (sortType == "merge") {
            return new MergeSort();
        }
        return new BubbleSort();
    }

//    static void sortAndPrintArray(int[] array, BubbleSort arraySort) {
//        arraySort.sort(array);
//        for (int i = 0; i < array.length; i++) {
//            System.out.println(array[i]);
//        }
//    }
//
//    static void sortAndPrintArray(int[] array, MergeSort arraySort) {
//        arraySort.sort(array);
//        for (int i = 0; i < array.length; i++) {
//            System.out.println(array[i]);
//        }
//    }


}
